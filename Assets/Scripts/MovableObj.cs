using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovableObj : MonoBehaviour
{
    [SerializeField]
    private float moveTime = 0.1f;
    
    [SerializeField]
    public LayerMask blockingLayer;

    private BoxCollider2D _boxCollider2D;
    private Rigidbody2D _rb2D;

    public float iinverseMoveTime;

    protected virtual void Start()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _rb2D = GetComponent<Rigidbody2D>();
        iinverseMoveTime = iinverseMoveTime;
        //iinverseMoveTime = iinverseMoveTime / moveTime;
        
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir, yDir);

        _boxCollider2D.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        _boxCollider2D.enabled = true;

        if (hit.transform == null)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemDistance = (transform.position - end).sqrMagnitude;

        while (sqrRemDistance > float.Epsilon)
        {
            Vector3 newPos =
                Vector3.MoveTowards(_rb2D.position, end, iinverseMoveTime * Time.deltaTime);
            _rb2D.MovePosition(newPos);
            sqrRemDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    protected virtual void AttempMove<T>(int xDir, int yDir) where T : Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);
        
        if(hit.transform == null)
            return;

        T hitComponent = hit.transform.GetComponent<T>();
        
        if(canMove == false && hitComponent != null)
        {
            OncanMove(hitComponent);
        }
    }

    protected abstract void OncanMove<T>(T component) where T : Component;
}
