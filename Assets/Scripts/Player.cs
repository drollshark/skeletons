using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    [SerializeField]
    private bool isGameOver = false;
    
    private Vector3 _targetPoint, _origPos;
    [SerializeField] private bool canMove;
    [SerializeField] private bool _canAttack;

    [SerializeField]
    private float timeToMove = 0.2f;
    
    [SerializeField]
    private float timeToAttack = 0.2f;
    
    [SerializeField]
    private float _attackRange = 0.2f;

    [SerializeField] private LayerMask stopMovementObj;
    [SerializeField] private LayerMask _enemyLayer;

    [SerializeField]
    private SpriteRenderer _playerSpriteRenderer;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private Transform _leftAttackPos, _rightAttackPos;
    
    public static event  Action <GameObject, GameObject> attack; 
    protected  void Start()
    {
        // синхронизация параметров с GameManager
        //Animator
    }

    private void OnDisable()
    {
        //Синхранизация параметров с GameManager
    }
    
    private void Update()
    {
        CheckIsGameOver();
        InputHandler();
    }

    void InputHandler()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && canMove)
            StartCoroutine(MovePlayer(Vector3.up));

        if (Input.GetKeyDown(KeyCode.DownArrow) && canMove)
            StartCoroutine(MovePlayer(Vector3.down));

        if (Input.GetKeyDown(KeyCode.LeftArrow) && canMove)
        {
            StartCoroutine(MovePlayer(Vector3.left));
            
            if (_playerSpriteRenderer.flipX == false)
            {
                _playerSpriteRenderer.flipX = true;
            }
            else
            {
                return;
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && canMove)
        {
            StartCoroutine(MovePlayer(Vector3.right));
            
            if (_playerSpriteRenderer.flipX == true)
            {
                _playerSpriteRenderer.flipX = false;
            }
            else
            {
                return;
            }
        }
        
        if(Input.GetKeyDown((KeyCode.R)))
            RestartLevel();

        if (Input.GetKeyDown((KeyCode.Space)))
        {
            Attack();
        }

    }

    void Attack()
    {

        Vector3 direction;
        GameObject _enemy = null;

        if (_canAttack == true)
        {
            if (_playerSpriteRenderer.flipX == true)
            {
                Collider2D hit;
                hit = Physics2D.OverlapCircle(_leftAttackPos.position, _attackRange, _enemyLayer);

                if (hit)
                {
                    _enemy = hit.gameObject;
                
                    print(_enemy.name);
                
                    if (_enemy != null && _enemy.GetComponent<EnemyController>())
                    {
                        StartCoroutine(Attack(_enemy));
                    } 
                }
            }

            if (_playerSpriteRenderer.flipX == false)
            {
                Collider2D hit;

                hit = Physics2D.OverlapCircle(_rightAttackPos.position, _attackRange, _enemyLayer);

                if (hit)
                {
                    _enemy = hit.gameObject;
                
                    print(_enemy.name);
                
                    if (_enemy != null)
                    {
                        StartCoroutine(Attack(_enemy));
                    } 
                } 
            }
            
        }
        
    }

    private IEnumerator MovePlayer(Vector3 direction)
    {
        canMove = false;
        _animator.SetBool("Walk", true);
        float elepsedTime = 0;
        
        _origPos = transform.position;
        _targetPoint = _origPos + direction;

        if (CheckBlock(_targetPoint) == false)
        {
            while (elepsedTime < timeToMove)
            {
                transform.position = Vector3.Lerp(_origPos, _targetPoint, (elepsedTime / timeToMove));
                elepsedTime += Time.deltaTime;
                yield return null;
            }

            transform.position = _targetPoint;
        }

        _animator.SetBool("Walk", false);
        canMove = true;
        
    }
    
    private IEnumerator Attack(GameObject _enemy)
    {
        _canAttack = false;
        canMove = false;
        
        _animator.SetBool("Attack", true);
        
        float elepsedTime = 0;
        
        attack?.Invoke(gameObject, _enemy);
        while (elepsedTime < timeToAttack)
        {
            
            elepsedTime += Time.deltaTime;
            yield return null;
        }
        
        _animator.SetBool("Attack", false);
        _canAttack = true;
        canMove = true;
        
    }

    private bool CheckBlock(Vector3 direction)
    {
        bool isBlock;

        isBlock = Physics2D.OverlapCircle(direction, 0.2f, stopMovementObj);

        return isBlock;
    }
    
    void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }

    void CheckIsGameOver()
    {
        //Условие проигрыша
        if (isGameOver == true)
        {
            canMove = false;
            GameManager.instance.GameOver();
        }
    }
    
}
