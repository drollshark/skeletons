using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum States
    {
        Zero,
        One,
        Two,
        Three,
        Dead,
        Run
    }
    
    public  States _states;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private float _timeToChangeState_1_to_2;
    
    [SerializeField]
    private float _timeToChangeState_2_to_3;

    public void SelectState(States states)
    {
        print("Состояние cйчас " + _states + ", Будет изменино на " + states);
        
        _states = states;
        
        print(_states);
    }

    private void Awake()
    {
        _states = States.Zero;
    }

    private void Update()
    {
        StateUpdate();
    }

    private void StateUpdate()
    {
        switch (_states)
        {
            default:
            
            case States.Zero:
                break;

            case States.One:
                StartCoroutine(OneState());
                break;
            
            case States.Two:
                StartCoroutine(TwoState());
                break;
            
            case States.Three:
                StartCoroutine(ThreeState());
                break;
            
            case States.Dead:
                gameObject.SetActive(false);
                break;
        }
    }

    private IEnumerator OneState()
    {
        
        _animator.SetBool("OneState", true);
        
        float elepsedTime = 0;
        while (elepsedTime < _timeToChangeState_1_to_2)
        {
            elepsedTime += Time.deltaTime;
            yield return null;
        }

        _states = States.Two;

    }
    
    private IEnumerator TwoState()
    {
        
        _animator.SetBool("OneState", false);
        _animator.SetBool("TwoState", true);
        
        float elepsedTime = 0;
        while (elepsedTime < _timeToChangeState_2_to_3)
        {
            elepsedTime += Time.deltaTime;
            yield return null;
        }
        
        _states = States.Three;

    }
    
    private IEnumerator ThreeState()
    {
        
        _animator.SetBool("TwoState", false);
        _animator.SetBool("ThreeState", true);
        
        float elepsedTime = 0;
        while (elepsedTime < _timeToChangeState_2_to_3)
        {
            elepsedTime += Time.deltaTime;
            yield return null;
        }

        _states = States.Run;
    }
}
