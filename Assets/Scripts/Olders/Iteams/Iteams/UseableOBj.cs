using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseableOBj : MonoBehaviour
{
   [SerializeField]
   private GameObject Icons;

   private bool isShow = false;
   private void Start()
   {
      Icons.gameObject.SetActive(false);
   }

   public void ShowIcons()
   {
      Icons.gameObject.SetActive(true);
      isShow = true;
      StartCoroutine(TimerHide());
   }

   public void HideIcons()
   {
      isShow = false;
      Icons.gameObject.SetActive(false);
   }

   public void DesstroySelf()
   {
      Destroy(gameObject);
   }

   IEnumerator TimerHide()
   {
      if (isShow == true)
      {
         yield return new WaitForSeconds(1.5f);
         HideIcons();
      }
      
   }
}
