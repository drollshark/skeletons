using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public GameObject _GameManager;
    
    private void Awake()
    {
        if (_GameManager != null)
        {
            Instantiate(_GameManager);
        }
    }
}
