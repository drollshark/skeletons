using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [SerializeField]
    private Graystones_Manager _manager;

    public bool playerCanMove = true;
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if(instance != null)
            Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);

        _manager = GetComponent<Graystones_Manager>();
    }

    private void Start()
    {
       InitGame();
    }

    void InitGame()
    {
        print("Игра началась " + instance);

        int num;
        num = Random.Range(1, 4);
        for (int i = 0; i < num; i++)
        {
            _manager.SpawnEnemy();
        }
    }

    public void GameOver()
    {
        print("Игра окончена !");
    }
}
