using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Graystones_Manager : MonoBehaviour
{
    [SerializeField] // Для Debug
    private List<GameObject> _fullGraystones;
    
    [SerializeField] // Для Debug
    private List<GameObject> _emptyGraystone; 
    
    [SerializeField]
    private GameObject _enemyPref;

    [Range(1,10)]
    [SerializeField]
    private float _timeToRespawn;

    private void OnEnable()
    {
        Player.attack += PlayerOnattack;
    }

    private void PlayerOnattack(GameObject sender, GameObject enemy)
    {
        KillEnemy(sender, enemy);
    }

    private void Awake()
    {
        GameObject[] spawns;
        spawns = GameObject.FindGameObjectsWithTag("Spawn");

        for (int i = 0; i < spawns.Length; i++)
        {
            _emptyGraystone.Add(spawns[i]);
        }
    }

    private void FixedUpdate()
    {
        StartCoroutine(SpawnCoolDown());
    }

    private IEnumerator SpawnCoolDown()
    {
        float elepsedTime = 0;
        while (elepsedTime < _timeToRespawn)
        {
            elepsedTime += Time.deltaTime;
            yield return null;
        }

        int num = Random.Range(1, 3);
        for (int i = 0; i < num; i++)
        {
            SpawnEnemy();
        }
    }
    
    public void SpawnEnemy()
    {
        if (_emptyGraystone.Count > 0 && _emptyGraystone != null)
        {
            Transform spawn;
            spawn = GetEmptySpawn().transform;

            bool turnLeft = false;
            turnLeft = spawn.parent.gameObject.GetComponent<SpriteRenderer>().flipX;
        
            GameObject _enemy = Instantiate(_enemyPref, spawn);
        
            EnemyController childObj;
            childObj = _enemy.GetComponent<EnemyController>();

            childObj.GetComponentInChildren<SpriteRenderer>().flipX = !turnLeft;
            childObj.SelectState(EnemyController.States.One); 
        }
        
    }

    public void KillEnemy(GameObject sender, GameObject obj)
    {
        AddSpawnZone(obj);
        Destroy(obj);
    }

    private void GetSpawn(GameObject obj)
    {
        // GameObject rootObj;
        // rootObj = obj.gameObject.CompareTag("SpawnHolder").;
        // return;
        
    }

    void AddSpawnZone(GameObject _gameObject)
    {
        if (_fullGraystones.Count > 0 && _fullGraystones != null)
        {
            _fullGraystones.Remove(_gameObject);
            _emptyGraystone.Add(_gameObject);
        }
    }
    
    private GameObject GetEmptySpawn()
    {
        int randomEmptyGray = Random.Range(0, _emptyGraystone.Count - 1);
        GameObject _emptySpawn = _emptyGraystone[randomEmptyGray];
        
        _fullGraystones.Add(_emptySpawn);
        _emptyGraystone.Remove(_emptySpawn);
        
        return _emptySpawn;
        
    }
    
    
}
